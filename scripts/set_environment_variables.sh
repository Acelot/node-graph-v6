#!/bin/bash
#export RUSTFLAGS='--cfg tokio_unstable -Awarnings' 
#export RUSTFLAGS='-Awarnings -C target-feature=+avx,+avx2,+adx,+aes,+avx512bitalg,+avx512bw,+avx512cd,+avx512dq,+avx512f,+avx512ifma,+avx512vbmi,+avx512vbmi2,+avx512vl,+avx512vnni,+avx512vpopcntdq,+bmi1,+bmi2,+cmpxchg16b,+ermsb,+f16c,+fma,+fxsr,+gfni,+lzcnt,+movbe,+pclmulqdq,+popcnt,+rdrand,+rdseed,+sha,+sse,+sse2,+sse3,+sse4.1,+sse4.2,+ssse3,+vaes,+vpclmulqdq,+xsave,+xsavec,+xsaveopt,+xsaves' 
#export RUSTFLAGS='-Awarnings -C target-cpu=native'
