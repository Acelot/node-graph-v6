#!/bin/bash
cargo bench --no-run --target-dir ./target/benchmark_target  || { echo 'Build failed' ; exit 1; }
perf stat -d -d -d cargo bench --bench benchmark -- --profile-time 1
# https://github.com/bheisler/criterion.rs/issues/754#issuecomment-1928015159
