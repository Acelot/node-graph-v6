# (WIP) Unnamed Node Graph Library V6

## About

"Unnamed node graph library" is the result of a thought experiment, which asked the question: "Can you express everything in terms of a node graph?" 

Foregoing the question: "Is this a good idea?".

## Architecture

The whole library revolves around the `Node`/`NodeInner` type. It is essentially a trait object implemented as a struct for ABI stabiliy & increased control. 

This node graph library implements different back-ends (`src/node/node_implementations`) which store the link/edges to other nodes. The idea is to put as much information into the nodes themselves.

For example consider a heap allocated u64 object. In terms of nodes you can descibe it as follows:

![Diagram](./notes/mermaid-diagram_1.png){height=25%}

The diagram above shows a simplefied version of a node "type", "`ObjectNodeType` from `object_node_type.rs`" To be specific.

A node "type" is essentially a tagged node with a specific nodes which have a specific tags as links. These `Node`s can be converted into a Node "type" wrapper to handles high level functionality (such as retrieving a object stored by a node object). See `src/node/node_types` for more info.

Technically speaking the only required dynamic function to implement Node is the `links` function. But for optimization purposes & ease of use, more methods are provided.

### Scripts

```
# Scripts and there watch variants (re-runs upon source change)
run_benchmark_perf.sh  - (requires perf & permissions)
run_benchmark.sh - Runs benchmark.
run_unit_tests.sh - Runs unit tests
```
Note, unit test use `nextest`.

`cargo install cargo-nextest --locked`

See https://nexte.st/book/installing-from-source.html for more info.

## API usage

For some quick/extensive API usage look at the unit tests **>>** (`src/tests/mod.rs`) **<<**.

### Quick start


```rust
// Create nods
let node1: Node = NodeBuilder::create_default_node();
let node2: Node = NodeBuilder::create_default_node();

// Link nodes (node1 -> node2)
Node::link(&node1, &node2, NodeLinkDirection::To);
// Get links of Node1
let mut links = Node::links(&node1);
let link = links.next();
assert!(link.is_some());
// link.node == node2
assert_eq!(link.unwrap().node, node2)
```

## Note

Documentation is TBD. Public API is unstable.

Do NOT use in production if you value your sanity.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
