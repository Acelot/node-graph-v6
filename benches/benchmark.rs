use criterion::{criterion_group, criterion_main, Criterion};

pub fn criterion_benchmark(c: &mut Criterion) {
    node::benchmark_node_implementations(c);
    id_generator_benchmark::benchmark_id_generator(c);
}

mod node {


    use criterion::{
        black_box, measurement::WallTime, Bencher, BenchmarkGroup, BenchmarkId, Criterion,
    };

    use node_graph_6::node::NodeLinkDirection;
    use node_graph_6::node::{
        node_implementations::node_hash_set_impl::NodeHashSet, Node, NodeDynamicFunctions,
    };
    use rand::seq::SliceRandom;
    use rand::{Rng, SeedableRng};

    

    pub struct BenchTestNodes<'a> {
        pub name:        &'a str,
        pub node_create: fn() -> Node,
    }

    impl<'a> BenchTestNodes<'a> {
        pub fn new(name: &'a str, node_create: fn() -> Node) -> Self {
            Self { name, node_create }
        }
    }
    #[derive(Clone)]
    struct NodesBenchmarkContainer(Vec<Node>);
    impl<'a> BenchTestNodes<'a> {
        fn create_batch(&self, sample_size: u64) -> NodesBenchmarkContainer {
            // TODO FIXME: Generate randomly linked nodes collection from seed.
            let vec: Vec<_> = (0..sample_size).map(|_| (self.node_create)()).collect();
            let mut rng = rand::rngs::StdRng::seed_from_u64(42);
            for node in &vec {
                let random_node = vec.choose(&mut rng).unwrap();
                let direction = match rng.r#gen(){
                    true => NodeLinkDirection::To,
                    false => NodeLinkDirection::From
                };
                
                node.link(random_node, direction);
            }

            NodesBenchmarkContainer(vec)
        }
    }

    impl Drop for NodesBenchmarkContainer {
        fn drop(&mut self) {
            for i in self.0.iter(){
                i.destroy();
            }
        }
    }

    pub fn benchmark_node_implementations(c: &mut Criterion) {
        // Batch sample sizes
        let sample_sizes = &[10];

        // Node implementations
        let node_types = [BenchTestNodes::new("DashSet Node", || {
            NodeHashSet::default().into()
        })];

        // FIXME: Put in array.

        run_benchmarks(
            &node_types,
            sample_sizes,
            c.benchmark_group("links_bench"),
            links_bench,
        );

        run_benchmarks(
            &node_types,
            sample_sizes,
            c.benchmark_group("has_link_bench"),
            has_link_bench,
        );

        run_benchmarks(
            &node_types,
            sample_sizes,
            c.benchmark_group("link_bench"),
            link_bench,
        );
        run_benchmarks(
            &node_types,
            sample_sizes,
            c.benchmark_group("unlink_bench"),
            unlink_bench,
        );
        run_benchmarks(
            &node_types,
            sample_sizes,
            c.benchmark_group("get_link_bench"),
            get_link_bench,
        );
        run_benchmarks(
            &node_types,
            sample_sizes,
            c.benchmark_group("unlink_all_bench"),
            destroy_bench,
        );
        run_benchmarks(
            &node_types,
            sample_sizes,
            c.benchmark_group("get_debug_name_bench"),
            get_debug_name_bench,
        );
    }

    fn run_benchmarks<R>(
        node_types: &[BenchTestNodes<'_>],
        sample_sizes: &[u64],
        mut contains_bench_group: BenchmarkGroup<'_, WallTime>,
        mut benchmarking_function: R,
    ) where
        R: (FnMut(&NodesBenchmarkContainer)) + Copy,
    {
        for node_test_data in node_types {
            for sample_size in sample_sizes {

                contains_bench_group.bench_with_input(
                    BenchmarkId::new(node_test_data.name, *sample_size),
                    &node_test_data.create_batch(*sample_size),
                    |bencher: &mut Bencher, nodes: &NodesBenchmarkContainer| bencher.iter(||benchmarking_function(nodes)),
                );
            }
        }
    }

    fn links_bench(nodes: &NodesBenchmarkContainer) {
      
        for node_a in &nodes.0 {
            for node_b in &nodes.0 {
                black_box(node_a);
                black_box(node_b);
            }
        }
    }
    fn has_link_bench(nodes: &NodesBenchmarkContainer) {
      
        for node_a in &nodes.0 {
            for node_b in &nodes.0 {
                black_box(node_a.has_link(node_b, NodeLinkDirection::To));
            }
        }
    }
    fn link_bench(nodes: &NodesBenchmarkContainer) {
      
        for node_a in &nodes.0 {
            for node_b in &nodes.0 {
                black_box(node_a.link(node_b, NodeLinkDirection::To));
            }
        }
    }
    fn unlink_bench(nodes: &NodesBenchmarkContainer) {
      
        for node_a in &nodes.0 {
            for node_b in &nodes.0 {
                black_box(node_a.unlink(node_b, NodeLinkDirection::To));
            }
        }
    }
    fn get_link_bench(nodes: &NodesBenchmarkContainer) {
      
        for node_a in &nodes.0 {
            for node_b in &nodes.0 {
                black_box(node_a.get_link(node_b.get_id(), NodeLinkDirection::To));
            }
        }
    }
    fn destroy_bench(nodes: &NodesBenchmarkContainer) {
      
        for node_a in &nodes.0 {
            black_box(node_a.destroy());
        }
    }
    fn get_debug_name_bench(nodes: &NodesBenchmarkContainer) {
      
        for node_a in &nodes.0 {
            black_box(node_a.get_debug_name());
        }
    }
}

mod id_generator_benchmark {
    use criterion::{Bencher, Criterion};
    use node_graph_6::id_generator::IdGenerator;

    pub fn benchmark_id_generator(c: &mut Criterion) {
        c.bench_function("Id generator (reuse) benchmark", benchmark_speed_resuse);
        c.bench_function("Id generator benchmark", benchmark_speed);
    }

    pub fn benchmark_speed_resuse(bencher: &mut Bencher) {
        let id_generator = IdGenerator::default();
        bencher.iter(|| {
            let id = id_generator.get_id();
            id_generator.reuse_id(id);
        })
    }

    pub fn benchmark_speed(bencher: &mut Bencher) {
        let id_generator = IdGenerator::default();
        bencher.iter(|| {
            id_generator.get_id();
        })
    }
}

criterion_group!(benches, node::benchmark_node_implementations,    id_generator_benchmark::benchmark_id_generator);
criterion_main!(benches);
