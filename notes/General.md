# General Notes

## Todo:
- Unit test `node types` 
## Reminder
- Write benchmarks
- Write unit tests
- Write "Node Type" Types
## Note
```rust
enum TestEnum {
    EnumA(bool),
    EnumB(bool),
}
let my_bool = true;
let (TestEnum::EnumA(my_bool)| TestEnum::EnumB(my_bool)) = my_enum
```
https://discord.com/channels/273534239310479360/818964227783262209/1211682603527049227
### Case 3 - Poison 
    Rwlock can be poisoned.
    Arc too?
    Write tests and deal with such cases.

### Case 2 - dlopen/JIT
```
    | Yand!rs
```
    
    https://discord.com/channels/273534239310479360/1120124565591425034/1207784384003051580

```
    If you don't unload/dlclose stuff, and you are careful about ABI-stability (i.e., no dyn Trait exactly, rather, a hand-made version thereof), and if you somehow manage to produce cross-compilation-unique TypeIds, then and only then yeah 😅
    So I guess the answer to nice is actually not so much
```

    
```
    Regarding ABI stability:Regarding ABI stability
    https://docs.rs/abi_stable
    https://docs.rs/stabby
    
```

### Case 1 - The "Rust type id duplicates node" - The time I forgot and remembered the possibility for a edge case.
Difference between Node A & B = different link storage backing.

-Create Object node With Type #1-
Create Node A. (Rust Type Id #1)
Link Node A to "Opaque Object node OBJECT #1".
Link Node A to "Rust Type Ids".

-Create Object node With Type #1-
Create Node B. (Rust Type Id #1)
Link Node B to "Opaque Object node OBJECT #2".
Link Node B to "Rust Type Ids".
\[LINK Will be ignored as it already has a link with the same id \]

Links:

Node A - "Opaque Object node OBJECT #1"
Node A - "Rust Type Ids"

Node B - "Opaque Object node OBJECT #2"

## Tests
    Making node_inner -> <RwLock<Arc<RwLock<NodeInner>> is a bad idea.