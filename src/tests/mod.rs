mod node_types {
    mod object_node_type {

        use crate::node::{
            node_implementations::{
                node_hash_set_impl::NodeHashSet, node_object_impl::ObjectNodeImpl,
            },
            node_types::object_node_type::ObjectNodeType,
            Node, NodeBuilder,
        };

        #[test]
        fn from_fn() {
            type ObjectType = i32;
            type NodeImplType = NodeHashSet;
            type ResultingObjectType = ObjectNodeImpl<ObjectType, NodeImplType>;

            let node_a: Node = NodeBuilder::default()
                .node_object_default(42_i32)
                .build_object_type_node();

            ObjectNodeType::<ResultingObjectType>::from(&node_a).unwrap();
        }
        #[test]
        fn get_object_fn() {
            type ObjectType = i32;
            let object = 42_i32;
            let node_a: Node = NodeBuilder::default()
                .node_object_default(object)
                .build_object_type_node();
            let object_node_type_instance = ObjectNodeType::<ObjectType>::from(&node_a).unwrap();
            let object_from_node = object_node_type_instance.get_object();
            assert_eq!(object, *object_from_node);
        }

        #[test]
        fn get_object_with_name_fn() {
            type ObjectType = i32;
            let object = 42_i32;
            let node_a: Node = NodeBuilder::create_default_object_node_with_name(
                "Test",
                object,
            );
            let object_node_type_instance = ObjectNodeType::<ObjectType>::from(&node_a).unwrap();
            let object_from_node = object_node_type_instance.get_object();
            assert_eq!(object, *object_from_node);
        }
    }

    mod string_node_tag {
        use crate::node::{node_types::string_node_tag::NameNodeType, NodeBuilder};

        #[test]
        fn from_node_fn() {
            let name = "Unit test node name";
            let node_a = NodeBuilder::default()
                .name(name)
                .node_id_opaque()
                .node_hash_set()
                .build_generic_node();
            let name_type_node = NameNodeType::get_name_node(&node_a).unwrap();

            assert_eq!(name, name_type_node.get_name())
        }

        #[test]
        fn from_name_fn() {
            let name = "Unit test node name";
            let _ = NodeBuilder::default()
                .name(name)
                .build_name_type_node();
            NameNodeType::from_name(name).unwrap();
        }

        #[test]
        fn get_name_fn() {
            let name = "Unit test node name";
            let node_a = NodeBuilder::default()
                .name(name)
                .node_id_opaque()
                .node_hash_set()
                .build_generic_node();
            let name_type_node_a = NameNodeType::from_name(name).unwrap();
            let name_type_node_b = NameNodeType::get_name_node(&node_a).unwrap();
            assert_eq!(name_type_node_a.get_name(), name);
            assert_eq!(name_type_node_a.0, name_type_node_b.0)
        }

        #[test]
        fn get_name_with_object() {
            let name = "Unit test node name";
            let node_a = NodeBuilder::default()
                .name(name)
                .node_object_default(1)
                .build_object_type_node();
            let name_type_node_a = NameNodeType::from_name(name).unwrap();
            let name_type_node_b = NameNodeType::get_name_node(&node_a).unwrap();
            assert_eq!(name_type_node_a.get_name(), name);
            assert_eq!(name_type_node_a.0, name_type_node_b.0)
        }
    }

    // TODO: Implement the functions below.
    mod link_node_tag {
        // use crate::node::{node_implementations::node_hash_set_impl::NodeHashSet, Node};
        // TODO
        // The graph specification isn't final yet and this isn't needed just yet.

        // #[test]
        // fn from_fn() {
        //     let node_a: Node = NodeHashSet::default().into();
        //     let node_b: Node = NodeHashSet::default().into();
        // }
        // #[test]
        // fn get_directional_to_links_fn() {
        //     let node_a: Node = NodeHashSet::default().into();
        //     let node_b: Node = NodeHashSet::default().into();
        // }
        // #[test]
        // fn get_directional_to_one_link_fn() {
        //     let node_a: Node = NodeHashSet::default().into();
        //     let node_b: Node = NodeHashSet::default().into();
        // }
    }
}
mod node_impls {

    pub mod dynamic_functions_tests {

        

        use crate::node::{Node, NodeDynamicFunctions, NodeLinkDirection};

        pub fn default(create_node: impl Fn() -> Node) {
            let _node: Node = create_node();
        }

        pub fn links_fn(create_node: impl Fn() -> Node) {
            let node1: Node = create_node();
            let node2: Node = create_node();

            Node::link(&node1, &node2, NodeLinkDirection::To);

            let mut links = Node::links(&node1);
            let link = links.next();
            assert!(link.is_some());
            assert_eq!(link.unwrap().node, node2)
        }

        pub fn has_link_fn_link_single_fn_unlink_single_fn(create_node: impl Fn() -> Node) {
            let node1: Node = create_node();
            let node2: Node = create_node();

            assert!(Node::link_single(&node1, &node2, NodeLinkDirection::To));
            assert!(Node::link_single(&node2, &node1, NodeLinkDirection::To));
            
            assert!(Node::has_link(&node1, &node2, NodeLinkDirection::To));
            assert!(Node::has_link(&node2, &node1, NodeLinkDirection::To));

            
            assert!(Node::unlink_single(&node1, &node2, NodeLinkDirection::To));
            assert!(Node::unlink_single(&node2, &node1, NodeLinkDirection::To));
            
            assert!(!Node::has_link(&node1, &node2, NodeLinkDirection::To));
            assert!(!Node::has_link(&node2, &node1, NodeLinkDirection::To));
        }

        pub fn has_link_fn_link_fn_unlink_fn(create_node: impl Fn() -> Node) {
            let node_1: Node = create_node();
            let node_2: Node = create_node();

            let (node_1_result, node_2_result) = Node::link(&node_1, &node_2, NodeLinkDirection::To);
            assert!(node_1_result);
            assert!(node_2_result);
            assert!(Node::has_link(&node_1, &node_2, NodeLinkDirection::To));
            assert!(Node::has_link(&node_2, &node_1, NodeLinkDirection::From));

            let (node_1_result, node_2_result) = Node::unlink(&node_1, &node_2, NodeLinkDirection::To);
            assert!(node_1_result);
            assert!(node_2_result);
            assert!(!Node::has_link(&node_1, &node_2, NodeLinkDirection::To));
            assert!(!Node::has_link(&node_2, &node_1, NodeLinkDirection::To));
        }

        pub fn get_link_fn(create_node: impl Fn() -> Node) {
            let node1: Node = create_node();
            let node2: Node = create_node();

            Node::link(&node1, &node2, NodeLinkDirection::To);

            let found_node =
                Node::get_link(&node1, &Node::get_id(&node2), NodeLinkDirection::To).expect("Couldn't Find Node");

            assert_eq!(found_node, node2, "Nodes do not match.");
        }

        pub fn destroy_fn(create_node: impl Fn() -> Node) {
            let node1: Node = create_node();
            let node2: Node = create_node();

            Node::link(&node1, &node2, NodeLinkDirection::To);
            Node::destroy(&node1);

            assert!(Node::links(&node1).next().is_none());
        }

        pub fn get_node_name(create_node: impl Fn() -> Node, expected_debug_name: String) {
            let node1: Node = create_node();
            dbg!(&node1);
            assert_eq!(Node::get_node_name(&node1).unwrap(), expected_debug_name)
        }

        pub fn multi_threading(create_node: impl Fn() -> Node) {
            let node1 = create_node();
            let node2 = create_node();

            let node1_ = node1.clone();
            let node2_ = node2.clone();
            std::thread::spawn(move || node1.has_link(&node2, NodeLinkDirection::To));
            std::thread::spawn(move || node1_.has_link(&node2_, NodeLinkDirection::To));
        }

        pub fn multi_threading_panic(create_node: impl Fn() -> Node) {
            let node1 = create_node();
            let node2 = create_node();

            let node1_ = node1.clone();
            let node2_ = node2.clone();
            let t2 = std::thread::spawn(move || {
                node1.has_link(&node2, NodeLinkDirection::To);
                panic!();
            });            
            
            let t1 = std::thread::spawn(move || {
                node1_.has_link(&node2_, NodeLinkDirection::To);
            });            
            t1.join().unwrap();
            t2.join().unwrap_err();
        }
    }

    mod node_object {
        use crate::node::{
            node_implementations::{
                node_hash_set_impl::NodeHashSet, node_object_impl::ObjectNodeImpl,
            },
            Node, NodeBuilder,
        };

        #[test]
        fn creation() {
            let object = 1337;
            let node_object: Node = NodeBuilder::default()
                .node_object_default(object)
                .build_object_type_node();

            assert_eq!(
                node_object
                    .get_data::<ObjectNodeImpl<i32, NodeHashSet>>()
                    .object,
                object.into()
            );
        }
    }

    mod node_dash_set {
        // node_dash_set_impl doesn't require special checks.
    }

    mod general {

        use crate::{
            node::{
                node_implementations::{
                    node_hash_set_impl::NodeHashSet, node_object_impl::ObjectNodeImpl,
                },
                Node, NodeBuilder,
            },
            tests::node_impls::dynamic_functions_tests,
        };

        fn create_node_dash_set() -> impl Fn() -> Node {
            || NodeBuilder::create_default_node()
        }

        type GenericObjectType = i32;
        fn create_node_object_generic() -> impl Fn() -> Node {
            || NodeBuilder::create_default_object_node(GenericObjectType::default())
        }

        #[test]
        fn default() {
            dynamic_functions_tests::default(create_node_dash_set());
            dynamic_functions_tests::default(create_node_object_generic())
        }

        #[test]
        fn get_debug_name() {
            dynamic_functions_tests::get_node_name(
                create_node_dash_set(),
                std::any::type_name::<NodeHashSet>().to_string(),
            );
            dynamic_functions_tests::get_node_name(
                create_node_object_generic(),
                std::any::type_name::<ObjectNodeImpl<GenericObjectType, NodeHashSet>>().to_string(),
            );
        }

        #[test]
        fn get_link_fn() {
            dynamic_functions_tests::get_link_fn(create_node_dash_set());
            dynamic_functions_tests::get_link_fn(create_node_object_generic());
        }

        #[test]
        fn has_link_fn_link_fn_unlink_fn() {
            dynamic_functions_tests::has_link_fn_link_fn_unlink_fn(create_node_dash_set());
            dynamic_functions_tests::has_link_fn_link_fn_unlink_fn(create_node_object_generic());
        }

        #[test]
        fn has_link_fn_link_single_fn_unlink_single_fn() {
            dynamic_functions_tests::has_link_fn_link_single_fn_unlink_single_fn(
                create_node_dash_set(),
            );
            dynamic_functions_tests::has_link_fn_link_single_fn_unlink_single_fn(
                create_node_object_generic(),
            );
        }

        #[test]
        fn links_fn() {
            dynamic_functions_tests::links_fn(create_node_dash_set());
            dynamic_functions_tests::links_fn(create_node_object_generic());
        }
        #[test]
        fn destroy_fn() {
            dynamic_functions_tests::destroy_fn(create_node_dash_set());
            dynamic_functions_tests::destroy_fn(create_node_object_generic());
        }
        #[test]
        fn multithreaded_simple_node() {
            dynamic_functions_tests::multi_threading(create_node_dash_set());
            dynamic_functions_tests::multi_threading(create_node_object_generic());
        }

        #[test]
        fn multithreaded_simple_node_panic() {
            dynamic_functions_tests::multi_threading_panic(create_node_dash_set());
            dynamic_functions_tests::multi_threading_panic(create_node_object_generic());
        }
    }
}

mod id_generator {
    use crate::{id_generator::IdGenerator, node::node_id::NodeId};

    #[test]
    fn basic() {
        let id_generator = IdGenerator::default();
        let id = id_generator.get_id();
        id_generator.reuse_id(id);
        let id = id_generator.get_id();
        assert_eq!(
            id,
            NodeId::new(crate::node::node_id::NodeIdTypes::Opaque, 0)
        );
    }
}

mod builder {
    use crate::node::NodeBuilder;

    #[test]
    fn building_generic_node() {
        let _node = NodeBuilder::create_default_node();
    }

    #[test]
    fn building_object_node() {
        let int = 1_i32;
        let _node = NodeBuilder::create_default_object_node(int);
    }
    #[test]
    fn building_string_node() {
        let name = "Node test name";
        let _node = NodeBuilder::default()
            .name(name)
            .node_hash_set()
            .build_name_type_node();
    }
}
