use std::ops::Deref;

use crate::node::{Node, NodeBuilder, NodeLinkDirection};

#[derive(Debug)]
pub struct Bookmarks {
    pub string_id_tag: BookmarkNode,
    pub pointer_id_tag: BookmarkNode,
    pub opaque_id_tag: BookmarkNode,
    pub rust_type_id_tag: BookmarkNode,
    pub ids_tag: BookmarkNode,
    pub links_node_tag: BookmarkNode,
    pub node_link_node_tag: BookmarkNode,
    pub link_properties_node_tag: BookmarkNode,
    pub directional_link_node_tag: BookmarkNode,
    pub graph_meta_data_tag: BookmarkNode,
    pub object_node_tag: BookmarkNode,
    pub tags: BookmarkNode,
    pub name_tag: BookmarkNode,
}

impl Bookmarks {
    pub fn setup_bookmarks_links() {
        let bookmarks = unsafe { crate::BOOKMARKS.deref() };
        bookmarks.opaque_id_tag             .0.link_all_one_direction(&[&bookmarks.ids_tag.0,&bookmarks.tags.0,],             NodeLinkDirection::From);
        bookmarks.string_id_tag             .0.link_all_one_direction(&[&bookmarks.ids_tag.0,&bookmarks.tags.0,],             NodeLinkDirection::From);
        bookmarks.pointer_id_tag            .0.link_all_one_direction(&[&bookmarks.ids_tag.0,&bookmarks.tags.0,],             NodeLinkDirection::From);
        bookmarks.rust_type_id_tag          .0.link_all_one_direction(&[&bookmarks.ids_tag.0,&bookmarks.tags.0,],             NodeLinkDirection::From);
        bookmarks.ids_tag                   .0.link_all_one_direction(&[&bookmarks.tags.0,],                                  NodeLinkDirection::From);
        bookmarks.links_node_tag            .0.link_all_one_direction(&[&bookmarks.tags.0,&bookmarks.graph_meta_data_tag.0,], NodeLinkDirection::From);
        bookmarks.node_link_node_tag        .0.link_all_one_direction(&[&bookmarks.tags.0,&bookmarks.graph_meta_data_tag.0,], NodeLinkDirection::From);
        bookmarks.link_properties_node_tag  .0.link_all_one_direction(&[&bookmarks.tags.0,&bookmarks.graph_meta_data_tag.0,], NodeLinkDirection::From);
        bookmarks.directional_link_node_tag .0.link_all_one_direction(&[&bookmarks.tags.0,&bookmarks.graph_meta_data_tag.0,], NodeLinkDirection::From);
        bookmarks.graph_meta_data_tag       .0.link_all_one_direction(&[&bookmarks.tags.0,&bookmarks.graph_meta_data_tag.0,], NodeLinkDirection::From);
        bookmarks.object_node_tag           .0.link_all_one_direction(&[&bookmarks.tags.0,],                                  NodeLinkDirection::From);
        bookmarks.tags                      .0.link_all_one_direction(&[&bookmarks.tags.0,&bookmarks.graph_meta_data_tag.0,], NodeLinkDirection::From);
        bookmarks.name_tag                  .0.link_all_one_direction(&[&bookmarks.tags.0,],                                  NodeLinkDirection::From);
    }
}

// Always call setup_bookmarks!
impl Default for Bookmarks {
    fn default() -> Self {
        Self {
            string_id_tag:             BookmarkNode::new("string_id_tag"),
            pointer_id_tag:            BookmarkNode::new("pointer_id_tag"),
            opaque_id_tag:             BookmarkNode::new("opaque_id_tag"),
            rust_type_id_tag:          BookmarkNode::new("rust_type_id_tag"),
            ids_tag:                   BookmarkNode::new("ids_tag"),
            links_node_tag:            BookmarkNode::new("links_node_tag"),
            node_link_node_tag:        BookmarkNode::new("node_link_node_tag"),
            link_properties_node_tag:  BookmarkNode::new("link_properties_node_tag"),
            directional_link_node_tag: BookmarkNode::new("directional_link_node_tag"),
            graph_meta_data_tag:       BookmarkNode::new("graph_meta_data_tag"),
            object_node_tag:           BookmarkNode::new("object_node_tag"),
            tags:                      BookmarkNode::new("tags"),
            name_tag:                  BookmarkNode::new("name_tag"),
        }
    }
}

#[derive(Debug)]
pub struct BookmarkNode(pub Node);

impl BookmarkNode {
    fn new(name: &str) -> Self {
        Self(NodeBuilder::default().debug_name(name).node_hash_set().node_id_opaque().build_generic_node())
    }
}
