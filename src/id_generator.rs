/// A simple, thread safe, id reusing, id generator.
use std::sync::{
    atomic::{AtomicUsize, Ordering},
    mpsc,
};

use crate::node::node_id::{NodeId, NodeIdTypes};

pub struct IdGenerator {
    // Not NodeId on purpose. Consider the case where NodeId changes type, the conversion from usize -> NodeId need to be added, but the counting mechanism/system will stay the same.
    next_id:              AtomicUsize,
    send_reusable_ids:    mpsc::Sender<NodeId>,
    receive_reusable_ids: mpsc::Receiver<NodeId>,
}

impl Default for IdGenerator {
    fn default() -> Self {
        let (set_reusable_ids, get_reusable_ids) = mpsc::channel::<NodeId>();
        Self {
            next_id:              Default::default(),
            send_reusable_ids:    set_reusable_ids,
            receive_reusable_ids: get_reusable_ids,
        }
    }
}
impl IdGenerator {
    /// Returns a unique NodeId.

    pub fn get_id(&self) -> NodeId {
        match self.receive_reusable_ids.try_recv() {
            Ok(id) => id,
            Err(_) => {
                // TODO: Verify that this code is correct & safe.
                let next_id = self.next_id.fetch_add(1, Ordering::Relaxed);
                NodeId::new(crate::node::node_id::NodeIdTypes::Opaque, next_id)
            }
        }
    }

    /// Called when a NodeId can be reused.
    pub fn reuse_id(&self, node_id: NodeId) {
        
        // Note it is required that NodeId's Type == Opaque
        assert_eq!(node_id.node_type, NodeIdTypes::Opaque);
        self.send_reusable_ids
            .send(node_id)
            .expect("Receiver has been deallocated, this should never happen.");
    }
}
