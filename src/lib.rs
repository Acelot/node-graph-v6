#[cfg(test)]
mod tests;


use tikv_jemallocator::Jemalloc;


#[global_allocator]
static GLOBAL: Jemalloc = Jemalloc;

pub mod bookmarks;
pub mod id_generator;
pub mod node;

use bookmarks::Bookmarks;
use id_generator::IdGenerator;
use node::{node_implementations::node_hash_set_impl::NodeHashSet, Node};
use once_cell::unsync::Lazy;

pub static mut BOOKMARKS: Lazy<Bookmarks> = Lazy::new(Bookmarks::default);
pub static mut ID_GENERATOR: Lazy<IdGenerator> = Lazy::new(IdGenerator::default);
pub static mut GLOBAL_NODE: Lazy<Node> = Lazy::new(|| NodeHashSet::default().into());
