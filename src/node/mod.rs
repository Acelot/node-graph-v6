pub mod node_id;
pub mod node_implementations;
pub mod node_types;

use crate::node::node_implementations::node_id_dummy_impl::DummyId;
use crate::node::{node_id::NodeIdEnumType, node_implementations::node_object_impl};
use crate::{BOOKMARKS, ID_GENERATOR};

use self::node_id::NodeId;
use self::node_implementations::node_hash_set_impl::NodeHashSet;
use self::node_types::object_node_type::ObjectNodeType;
use self::node_types::string_node_tag::NameNodeType;

use std::{any::Any, sync::Arc};
pub trait NodeDynamicFunctions {
    fn links(&self) -> LinksFnReturnType;
    fn has_link<'a>(
        &'a self,
        link: &'a Node,
        direction: NodeLinkDirection,
    ) -> HasLinkReturnType<'a>;
    fn link_single<'a>(
        &'a self,
        link: &'a Node,
        direction: NodeLinkDirection,
    ) -> LinkReturnType<'a>;
    fn unlink_single<'a>(
        &'a self,
        link: &'a Node,
        direction: NodeLinkDirection,
    ) -> UnLinkReturnType<'a>;
    fn get_link<'a>(
        &'a self,
        node_id: &'a NodeId,
        direction: NodeLinkDirection,
    ) -> GetLinkReturnType<'a>;
    fn destroy(&self) -> UnlinkAllReturnType;
    fn get_debug_name(&self) -> String;
}

// TODO: Consider inlining and removing the functions of node.
impl NodeDynamicFunctions for Node {
    fn links(&self) -> LinksFnReturnType {
        (self.node_inner.as_ref().unwrap().functions.links)(self)
    }

    fn has_link<'a>(
        &'a self,
        link: &'a Node,
        direction: NodeLinkDirection,
    ) -> HasLinkReturnType<'a> {
        (self.node_inner.as_ref().unwrap().functions.has_link)(self, link, direction)
    }

    fn link_single<'a>(
        &'a self,
        link: &'a Node,
        direction: NodeLinkDirection,
    ) -> LinkReturnType<'a> {
        (self.node_inner.as_ref().unwrap().functions.link_single)(self, link, direction)
    }

    fn unlink_single<'a>(
        &'a self,
        link: &'a Node,
        direction: NodeLinkDirection,
    ) -> UnLinkReturnType<'a> {
        (self.node_inner.as_ref().unwrap().functions.unlink_single)(self, link, direction)
    }

    fn get_link<'a>(
        &'a self,
        node_id: &'a NodeId,
        direction: NodeLinkDirection,
    ) -> GetLinkReturnType<'a> {
        (self.node_inner.as_ref().unwrap().functions.get_link)(self, node_id, direction)
    }

    fn destroy(&self) -> UnlinkAllReturnType {
        (self.node_inner.as_ref().unwrap().functions.destroy)(self)
    }

    fn get_debug_name(&self) -> String {
        (self.node_inner.as_ref().unwrap().functions.get_debug_name)(self)
    }
}

#[derive(Clone)]
pub struct Node {
    node_inner: Option<Arc<NodeInner>>,
    node_id:    NodeId,
}

#[derive(Default)]
pub struct NodeBuilder {
    object:           Option<Arc<dyn Any + Send + Sync>>,
    node_inner:       Option<NodeInner>,
    node_id:          Option<NodeId>,
    name:             Option<String>,
    debug_name:       Option<String>,
    empty_node_inner: bool,
}

impl NodeBuilder {
    pub fn name(&mut self, value: &str) -> &mut Self {
        self.name = Some(value.into());
        self
    }

    pub fn debug_name(&mut self, value: &str) -> &mut Self {
        self.debug_name = Some(value.into());
        self
    }

    pub fn object(&mut self, value: &Arc<dyn Any + Send + Sync>) -> &mut Self {
        self.object = Some(value.clone());
        self
    }

    pub fn node_inner<T: Into<NodeInner>>(&mut self, value: T) -> &mut Self {
        assert!(self.node_inner.is_none());
        self.node_inner = Some(value.into());
        self
    }

    pub fn node_object_template<T: 'static + Send + Sync>(
        &mut self,
        value: T,
        node_impl: impl NodeDynamicFunctions + Send + Sync + 'static,
    ) -> &mut Self {
        assert!(self.node_inner.is_none());
        let node_inner: NodeInner =
            node_object_impl::ObjectNodeImpl::new(node_impl, value.into()).into();
        self.node_inner = Some(node_inner);
        self
    }

    pub fn node_object_default<T: 'static + Send + Sync>(&mut self, value: T) -> &mut Self {
        assert!(self.node_inner.is_none());
        let object_arc: Arc<T> = value.into();
        self.object = Some(object_arc.clone());

        let node_inner: NodeInner =
            node_object_impl::ObjectNodeImpl::new(NodeHashSet::default(), object_arc).into();
        self.node_inner = Some(node_inner);
        self
    }

    pub fn node_hash_set(&mut self) -> &mut Self {
        assert!(self.node_inner.is_none());
        let node_inner: NodeInner = NodeHashSet::default().into();
        self.node_inner = Some(node_inner);
        self
    }

    pub fn node_dummy(&mut self) -> &mut Self {
        assert!(self.node_inner.is_none());
        self.empty_node_inner = true;
        self
    }

    pub fn node_id(&mut self, value: NodeId) -> &mut Self {
        assert!(self.node_id.is_none());
        self.node_id = Some(value);
        self
    }

    pub fn node_id_rust_type_generic<T: 'static>(&mut self) -> &mut Self {
        assert!(self.node_id.is_none());
        self.node_id = Some(NodeId::from_type::<T>());
        self
    }

    pub fn node_id_rust_type_id(&mut self) -> &mut Self {
        assert!(self.node_id.is_none());

        self.node_id = Some(NodeId::from_type_id(
            self.object.as_ref().expect("Object is not set!").type_id(),
        ));
        self
    }

    pub fn node_id_string<'a>(&mut self, value: String) -> &mut Self {
        assert!(self.node_id.is_none());
        self.node_id = Some(NodeId::from(value.as_str()));
        self
    }

    pub fn node_id_pointer<T: 'static>(&mut self, value: &T) -> &mut Self {
        assert!(self.node_id.is_none());
        self.node_id = Some(NodeId::from_ref(value));
        self
    }

    pub fn node_id_opaque(&mut self) -> &mut Self {
        assert!(self.node_id.is_none());
        let node_id = unsafe { ID_GENERATOR.get_id() };
        self.node_id = Some(node_id);
        self
    }

    pub fn build_object_type_node(&mut self) -> Node {
        // Relevant Node Type Wrapper:
        // node_types::object_node_type::ObjectNodeType
        assert!(self.object.is_some());
        assert!(
            self.node_id.is_none(),
            "Node Id is set, Object type nodes by design have pre-designated node ids."
        );
        self.node_id_opaque();
        let node = self.build_generic_node();

        node.link(unsafe { &BOOKMARKS.object_node_tag.0 }, NodeLinkDirection::From);

        let mut node_ptr_inner = DummyId::empty();
        node_ptr_inner.data = self.object.as_ref().unwrap().clone();
        node_ptr_inner.debug_name = "ObjectNode's \"True\" Object".to_owned();
        let node_ptr = Self::default()
            .object(&self.object.as_ref().unwrap())
            .node_id_pointer(self.object.as_ref().unwrap())
            .node_inner(node_ptr_inner)
            .build_generic_node();

        // Check if type node already exists, if so, get it & use it. If not, create a new one.
        let node_rust_type = match unsafe { &BOOKMARKS.rust_type_id_tag.0 }.get_link(
            &NodeId::from_type_id(self.object.as_ref().unwrap().type_id()),
            NodeLinkDirection::To,
        ) {
            Some(node) => node,
            None => Self::default()
                .object(self.object.as_ref().unwrap())
                .node_id_rust_type_id()
                .node_hash_set()
                .build_generic_node(),
        };

        node.link_single(&node_ptr, NodeLinkDirection::From);
        node.link(&node_rust_type, NodeLinkDirection::From);
        node
    }

    pub fn build_name_type_node(&mut self) -> Node {
        // Relevant Node Type Wrapper:
        // Name type node: A object node with a string id node attached to it.
        // node_types::string_node_tag::NameNodeType]
        assert!(
            self.node_id.is_none(),
            "Node Id is set, name type nodes by design have pre-designated node ids."
        );
        let name = self
            .name
            .as_ref()
            .expect("No name given for name node!")
            .clone();
        let string_node_id = Self::default()
            .node_hash_set()
            .node_id_string(name.to_string())
            .build_generic_node();

        let node = Self::default()
            .node_object_default(name)
            .build_object_type_node();

        node.link(unsafe { &BOOKMARKS.name_tag.0 }, NodeLinkDirection::From);
        node.link(&string_node_id, NodeLinkDirection::From);
        string_node_id.link(unsafe { &BOOKMARKS.string_id_tag.0}, NodeLinkDirection::From);

        node
    }

    pub fn build_generic_node(&mut self) -> Node {
        let node_id = std::mem::take(&mut self.node_id).expect("Didn't set Node Id!");
        let node_inner_option = std::mem::take(&mut self.node_inner);

        let node_inner = match node_inner_option {
            Some(mut node_inner) => {
                if let Some(name) = self.debug_name.as_ref() {
                    node_inner.debug_name = name.clone();
                }
                node_inner.node_id = node_id;
                Some(Arc::new(node_inner))
            }
            None => {
                assert!(self.empty_node_inner);
                None
            }
        };

        let node = Node {
            node_inner,
            node_id,
        };

        if let Some(name) = self.name.as_ref() {
            let string_node = Self::default().name(name).build_name_type_node();
            node.link(&string_node, NodeLinkDirection::From);
        }

        node
    }

    pub fn create_default_node_with_name(name: &str) -> Node {
        Self::default()
            .name(name)
            .node_hash_set()
            .node_id_opaque()
            .build_generic_node()
    }

    pub fn create_default_node() -> Node {
        Self::default()
            .node_hash_set()
            .node_id_opaque()
            .build_generic_node()
    }

    pub fn create_default_object_node_with_name<T: Send + Sync + 'static>(
        name: &str,
        object: T,
    ) -> Node {
        Self::default()
            .name(name)
            .node_object_default(object)
            .build_object_type_node()
    }

    pub fn create_default_object_node<T: Send + Sync + 'static>(object: T) -> Node {
        Self::default()
            .node_object_default(object)
            .node_id_opaque()
            .build_generic_node()
    }
}

impl std::hash::Hash for Node {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.node_id.hash(state);
    }
}

impl Eq for Node {}

impl PartialEq for Node {
    fn eq(&self, other: &Self) -> bool {
        self.node_id == other.node_id
    }
}
impl Node {
    pub fn get_node_name(&self) -> Option<String> {
        Some(self.get_debug_name())
    }

    pub fn get_data<T: 'static>(&self) -> &T {
        self.node_inner
            .as_ref()
            .unwrap()
            .data
            .downcast_ref::<T>()
            .unwrap() // TODO: Do downcast unchecked on release builds.
    }

    pub fn get_data_arc(&self) -> Arc<dyn Any + Send + Sync> {
        self.node_inner.as_ref().unwrap().data.clone() // TODO: Do downcast unchecked on release builds.
    }

    pub fn get_id(&self) -> &NodeId {
        &self.node_id
    }

    pub fn is_dummy(&self) -> bool {
        self.get_id().node_type == NodeIdEnumType::Pointer
    }

    pub fn link(&self, link: &Node, direction: NodeLinkDirection) -> (bool, bool) {
        let res = (
            Node::link_single(self, link, direction),
            Node::link_single(link, self, !direction),
        );
        // Fixme?: Add link variant: "Safe" link.
        // TODO: Remove asserts.
        debug_assert!(res.0);
        debug_assert!(res.1);
        res
    }

    pub fn unlink(&self, link: &Node, direction: NodeLinkDirection) -> (bool, bool) {
        (
            Node::unlink_single(self, link, direction),
            Node::unlink_single(link, self, !direction),
        )
    }

    pub fn get_links_with_link<'a>(
        &'a self,
        node: &'a Node, direction: NodeLinkDirection
        
    ) -> LinksFnReturnType {
        Box::new(Node::links(self).filter(move |link| !link.node.is_dummy() && link.node.has_link(node, direction)))
    }

    pub fn get_link_with_link<'a>(&'a self, node: &'a Node, direction: NodeLinkDirection) -> Node {
        let mut links = Node::links(self).filter(|link| link.node.has_link(node, direction));
        let link = links.next().unwrap();
        assert!(links.next().is_none());
        link.node
    }

    pub fn get_link_without_link<'a>(&'a self, node: &'a Node, direction: NodeLinkDirection) -> Node {
        let mut links = Node::links(self).filter(|link| link.node.has_link(node, direction));
        let link = links.next().unwrap();
        assert!(links.next().is_none());
        link.node
    }

    // TODO add unit test
    pub fn link_all(&self, links: &[&NodeLink]) {
        for node_link in links.iter() {
            self.link(&node_link.node, node_link.direction);
        }
    }

        // TODO add unit test
        pub fn link_all_one_direction(&self, links: &[&Node], direction: NodeLinkDirection) {
            for node in links.iter() {
                self.link(&node, direction);
            }
        }

    // TODO add unit test
    pub fn destroy_one(&self, links: &[&NodeLink]) {
        for node_link in links{
            self.unlink(&node_link.node, node_link.direction);
        }
    }

    fn none_clone(&self) -> Node{
        Node { node_inner: None, node_id: self.node_id }
    }

    pub fn get_nodes_directional(&self, direction: NodeLinkDirection) -> LinksFnReturnType{
        Box::new(self.links().filter(move |link|link.direction == direction))
    }
    pub fn get_node_directional(&self, direction: NodeLinkDirection) -> Node{
        let mut links = self.get_nodes_directional(direction);
        let node = links.next().unwrap().node;
        assert!(links.next().is_none());
        node
    }

    pub fn get_object_from_name<T: Send + Sync + 'static>(name: &'static str) -> Arc<T> {
        ObjectNodeType::from(&NameNodeType::from_name(name).unwrap().get_named_node()).unwrap().get_object()
    }

    pub fn get_node_from_name(name: &'static str) -> Node {
        NameNodeType::from_name(name).unwrap().get_named_node()
    }
}

impl std::fmt::Debug for Node {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Node")
            .field("Id", Node::get_id(self))
            .field("Node Debug Name", &Node::get_node_name(self))
            .finish()
    }
}

unsafe impl Send for Node {}
unsafe impl Sync for Node {}

pub struct NodeInner {
    data:       Arc<dyn Any + Send + Sync>,
    functions:  NodeFunctions, // Not often you see a Rwlock<Arc<T>> Instead of a Arc<RwLock<T>>.
    debug_name: String, // For debug measures only, name can be aquired through graph network.
    node_id:    NodeId, // Redundant but required because when this node is dropped it needs access to the NodeId.
}

impl Drop for NodeInner {
    fn drop(&mut self) {
        if self.node_id.node_type == NodeIdEnumType::Opaque {
            unsafe { ID_GENERATOR.reuse_id(self.node_id) }
        }
    }
}

impl Into<Node> for NodeInner {
    fn into(self) -> Node {
        let node_id = self.node_id.clone();
        Node {
            node_inner: Some(self.into()),
            node_id,
        }
    }
}

pub type LinksFnReturnType<'a> = Box<dyn Iterator<Item = NodeLink> + 'a>; // Returns linked nodes.
pub type HasLinkReturnType<'a> = bool; // Returns if has the link.
pub type LinkReturnType<'a> = bool; // Returns if it was already linked.
pub type UnLinkReturnType<'a> = bool; // True: Node is unlinked, False: Node to unlink didn't exist
pub type GetLinkReturnType<'a> = Option<Node>;
pub type UnlinkAllReturnType<'a> = ();

pub struct NodeFunctions {
    links:          fn(&Node) -> LinksFnReturnType,
    has_link: for<'a> fn(&'a Node, &'a Node, direction: NodeLinkDirection) -> HasLinkReturnType<'a>,
    link_single: for<'a> fn(&'a Node, &'a Node, direction: NodeLinkDirection) -> LinkReturnType<'a>,
    unlink_single:
        for<'a> fn(&'a Node, &'a Node, direction: NodeLinkDirection) -> UnLinkReturnType<'a>,
    get_link:
        for<'a> fn(&'a Node, &'a NodeId, direction: NodeLinkDirection) -> GetLinkReturnType<'a>,
    destroy:        fn(&Node) -> UnlinkAllReturnType,
    get_debug_name: fn(&Node) -> String,
    // Epiphany. What could I do if I could hook the get data function?
    // Not implementing util useful.
    // get_data:       RwLock<Arc<RwLock<fn(&Node) -> Arc<dyn Any + Send + Sync>>>>,
    // Though at that point making `Node` Rewritable would probably be better.
    // Node(Arc<Rwlock<NodeInner>) TODO benchmark, this could solve a lot of QOL problems.

    // Benchmark performance compared to removing nodes & inserting them with different functions.
}

#[derive(Debug, Clone, Hash, PartialEq, Eq, Copy)]
pub enum NodeLinkDirection {
    From,
    To,
}

impl std::ops::Not for NodeLinkDirection {
    type Output = Self;

    fn not(self) -> Self {
        match self {
            NodeLinkDirection::From => NodeLinkDirection::To,
            NodeLinkDirection::To => NodeLinkDirection::From,
        }
    }
}

#[derive(Debug, Clone, Hash, PartialEq, Eq)]
pub struct NodeLink {
    pub node:      Node,
    pub direction: NodeLinkDirection,
}

impl NodeLink {
    pub fn new(node: Node, direction: NodeLinkDirection) -> Self {
        Self { node, direction }
    }
}
