use std::sync::Arc;

use crate::node::{
    node_id::{self, NodeId}, GetLinkReturnType, HasLinkReturnType, LinkReturnType, LinksFnReturnType, Node, NodeDynamicFunctions, NodeFunctions, NodeInner, NodeLinkDirection, UnLinkReturnType, UnlinkAllReturnType
};

pub struct DummyId();

impl DummyId {
    // Constucts empty node inner for node building purposes.
    pub fn empty() -> NodeInner {
        NodeInner {
            data:       Arc::new(()),
            functions:  NodeFunctions {
                links:          DummyId::links,
                has_link:       DummyId::has_link,
                link_single:    DummyId::link_single,
                unlink_single:  DummyId::unlink_single,
                get_link:       DummyId::get_link,
                destroy:        DummyId::destroy,
                get_debug_name: DummyId::get_debug_name,
            },
            debug_name: "ObjectNode's \"True\" Object".to_owned(),
            node_id:    node_id::NodeId::new(node_id::NodeIdTypes::Invaild, 0),
        }
    }
}

impl From<&NodeId> for Node {
    fn from(value: &NodeId) -> Self {
        Node {
            node_inner: None,
            node_id:    *value,
        }
    }
}

impl DummyId {
    pub fn links(_: &Node) -> LinksFnReturnType {
        unimplemented!()
    }

    pub fn has_link<'a>(_: &'a Node, _: &'a Node, _: NodeLinkDirection) -> HasLinkReturnType<'a> {
        unimplemented!()
    }

    pub fn link_single<'a>(_: &'a Node, _: &'a Node, _: NodeLinkDirection) -> LinkReturnType<'a> {
        unimplemented!()
    }

    pub fn unlink_single<'a>(_: &'a Node, _: &'a Node, _: NodeLinkDirection) -> UnLinkReturnType<'a> {
        unimplemented!()
    }

    pub fn get_link<'a>(_: &'a Node, _: &'a NodeId, _: NodeLinkDirection) -> GetLinkReturnType<'a> {
        unimplemented!()
    }

    pub fn destroy(_: &Node) -> UnlinkAllReturnType {
        unimplemented!()
    }

    pub fn get_debug_name(_: &Node) -> String {
        unimplemented!()
    }
}

impl NodeDynamicFunctions for DummyId {
    fn links(&self) -> LinksFnReturnType {
        unimplemented!()
    }

    fn has_link<'a>(&self, _: &'a Node, _: NodeLinkDirection) -> HasLinkReturnType<'a> {
        unimplemented!()
    }

    fn link_single<'a>(&self, _: &'a Node, _: NodeLinkDirection) -> LinkReturnType<'a> {
        unimplemented!()
    }

    fn unlink_single<'a>(&self, _: &'a Node, _: NodeLinkDirection) -> UnLinkReturnType<'a> {
        unimplemented!()
    }

    fn get_link<'a>(&self, _: &'a NodeId, _: NodeLinkDirection) -> GetLinkReturnType<'a> {
        unimplemented!()
    }

    fn destroy(&self) -> UnlinkAllReturnType {
        unimplemented!()
    }

    fn get_debug_name(&self) -> String {
        unimplemented!()
    }
}
