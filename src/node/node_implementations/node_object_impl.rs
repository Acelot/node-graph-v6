use std::sync::Arc;

use crate::node::{
    node_id::NodeId,
    GetLinkReturnType, HasLinkReturnType, LinkReturnType, LinksFnReturnType, Node,
    NodeDynamicFunctions, NodeFunctions, NodeInner, NodeLinkDirection, UnLinkReturnType,
    UnlinkAllReturnType,
};

#[derive(Debug)]
pub struct ObjectNodeImpl<Object: Send + Sync, NodeImpl: NodeDynamicFunctions> {
    pub node:   NodeImpl,
    pub object: Arc<Object>, // TODO: Rewrite to: pub object: Object,
}

impl<Object: 'static + Send + Sync, NodeImpl: NodeDynamicFunctions + 'static + Send + Sync>
    ObjectNodeImpl<Object, NodeImpl>
{

    pub(crate) fn new(node: NodeImpl, object: Arc<Object>) -> Self {
        Self {
            node,
            object: object,
        }
    }
}

impl<O: 'static + Send + Sync, N: NodeDynamicFunctions + 'static + Send + Sync> Into<NodeInner>
    for ObjectNodeImpl<O, N>
{
    fn into(self) -> NodeInner {
        let node_inner = NodeInner {
            data:       Arc::new(self),
            functions:  NodeFunctions {
                links:          ObjectNodeImpl::<O, N>::links,
                has_link:       ObjectNodeImpl::<O, N>::has_link,
                link_single:    ObjectNodeImpl::<O, N>::link_single,
                unlink_single:  ObjectNodeImpl::<O, N>::unlink_single,
                get_link:       ObjectNodeImpl::<O, N>::get_link,
                destroy:        ObjectNodeImpl::<O, N>::destroy,
                get_debug_name: ObjectNodeImpl::<O, N>::get_debug_name,
            },
            debug_name: std::any::type_name::<ObjectNodeImpl<O, N>>().to_string(),
            node_id:    unsafe { crate::ID_GENERATOR.get_id() },
        };
        node_inner
    }
}

impl<O: 'static + Send + Sync, N: NodeDynamicFunctions + 'static + Send + Sync> Into<Node>
    for ObjectNodeImpl<O, N>
{
    fn into(self) -> Node {
        let node_inner: NodeInner = self.into();

        node_inner.into()
    }
}

impl<O: 'static + Send + Sync, N: NodeDynamicFunctions + 'static> ObjectNodeImpl<O, N> {
    fn links(node: &Node) -> LinksFnReturnType {
        let self_data = Node::get_data::<Self>(node);
        self_data.links()
    }

    fn has_link<'a>(
        node: &'a Node,
        link: &'a Node,
        direction: NodeLinkDirection,
    ) -> HasLinkReturnType<'a> {
        let self_data = Node::get_data::<Self>(node);
        self_data.has_link(link, direction)
    }

    fn link_single<'a>(
        node: &'a Node,
        link: &'a Node,
        direction: NodeLinkDirection,
    ) -> LinkReturnType<'a> {
        let self_data = Node::get_data::<Self>(node);
        self_data.link_single(link, direction)
    }

    fn unlink_single<'a>(
        node: &'a Node,
        link: &'a Node,
        direction: NodeLinkDirection,
    ) -> UnLinkReturnType<'a> {
        let self_data = Node::get_data::<Self>(node);
        self_data.unlink_single(link, direction)
    }

    fn get_link<'a>(
        node: &'a Node,
        node_id: &'a NodeId,
        direction: NodeLinkDirection,
    ) -> GetLinkReturnType<'a> {
        let self_data = Node::get_data::<Self>(node);
        self_data.get_link(node_id, direction)
    }

    fn destroy(node: &Node) -> UnlinkAllReturnType {
        let self_data = Node::get_data::<Self>(node);
        
        // Not Required and incorrect code for `NodeIdTypes::RustType` case.
        // for link in self_data.links() {
        //     match link.node.get_id().node_type {
        //         NodeIdTypes::Opaque => (),
        //         NodeIdTypes::String => link.node.destroy(),
        //         NodeIdTypes::Pointer => link.node.destroy(),
        //         NodeIdTypes::RustType => link.node.destroy(),
        //         NodeIdTypes::Invaild => assert!(
        //             false,
        //             "desonctructing a INVAILD NodeId type should never occur!"
        //         ),
        //     }
        // }
        self_data.destroy()
    }

    fn get_debug_name(node: &Node) -> String {
        // let self_data = Node::get_data::<Self>(node);
        // self_data.get_debug_name()
        node.node_inner.as_ref().unwrap().debug_name.clone()
    }
}

impl<O: 'static + Send + Sync, N: NodeDynamicFunctions + 'static> NodeDynamicFunctions
    for ObjectNodeImpl<O, N>
{
    fn links(&self) -> LinksFnReturnType {
        self.node.links()
    }

    fn has_link<'a>(&'a self, link: &'a Node, direction: NodeLinkDirection) -> HasLinkReturnType<'a> {
        self.node.has_link(link, direction)
    }

    fn link_single<'a>(&'a self, link: &'a Node, direction: NodeLinkDirection) -> LinkReturnType<'a> {
        self.node.link_single(link, direction)
    }

    fn unlink_single<'a>(&'a self, link: &'a Node, direction: NodeLinkDirection) -> UnLinkReturnType<'a> {
        self.node.unlink_single(link, direction)
    }

    fn get_link<'a>(&'a self, node_id: &'a NodeId, direction: NodeLinkDirection) -> GetLinkReturnType<'a> {
        self.node.get_link(node_id, direction)
    }

    fn destroy(&self) -> UnlinkAllReturnType {
        self.node.destroy()
    }

    fn get_debug_name(&self) -> String {
        unimplemented!()
        // std::any::type_name::<ObjectNodeImpl<O, N>>().to_string()
    }
}
