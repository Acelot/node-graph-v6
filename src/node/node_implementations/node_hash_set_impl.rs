use std::{
    collections::HashSet,
    sync::{Arc, RwLock},
};

use crate::node::{
    node_id::NodeId, GetLinkReturnType, HasLinkReturnType, LinkReturnType, LinksFnReturnType, Node, NodeDynamicFunctions, NodeFunctions, NodeInner, NodeLink, NodeLinkDirection, UnLinkReturnType, UnlinkAllReturnType
};

#[derive(Default, Debug)]
pub struct NodeHashSet {
    links: RwLock<HashSet<NodeLink>>,
}


impl NodeHashSet {
    fn links(node: &Node) -> LinksFnReturnType {
        let self_data = Node::get_data::<Self>(node);
        self_data.links()
    }

    fn has_link<'a>(node: &'a Node, link: &'a Node, direction: NodeLinkDirection) -> HasLinkReturnType<'a> {
        let self_data = Node::get_data::<Self>(node);
        self_data.has_link(link, direction)
    }

    fn link_single<'a>(node: &'a Node, link: &'a Node, direction: NodeLinkDirection) -> LinkReturnType<'a> {
        let self_data = Node::get_data::<Self>(node);
        self_data.link_single(link, direction)
    }

    fn unlink_single<'a>(node: &'a Node, link: &'a Node, direction: NodeLinkDirection) -> UnLinkReturnType<'a> {
        let self_data = Node::get_data::<Self>(node);
        self_data.unlink_single(link, direction)
    }

    fn get_link<'a>(node: &'a Node, node_id: &'a NodeId, direction: NodeLinkDirection) -> GetLinkReturnType<'a> {
        let self_data = Node::get_data::<Self>(node);
        self_data.get_link(node_id, direction)
    }

    fn destroy(node: &Node) -> UnlinkAllReturnType {
        let self_data = Node::get_data::<Self>(node);
        self_data.destroy()
    }

    fn get_debug_name(node: &Node) -> String {
        // let self_data = Node::get_data::<Self>(node);
        // self_data.get_debug_name()
        node.node_inner.as_ref().unwrap().debug_name.clone()
    }
}

impl NodeDynamicFunctions for NodeHashSet {
    fn links(&self) -> LinksFnReturnType {
        Box::new(self.links.read().unwrap().clone().into_iter())
        // Box::new(self.links().map(|node|node.clone()).into_iter()) // Alternative
    }

    fn has_link<'a>(&'a self, link: &'a Node, direction: NodeLinkDirection) -> HasLinkReturnType<'a> {
        
        self.links.read().unwrap().contains(&NodeLink::new(link.none_clone(), direction))
    }

    fn link_single<'a>(&'a self, link: &'a Node, direction: NodeLinkDirection) -> LinkReturnType<'a> {
        self.links.write().unwrap().insert(NodeLink::new(link.clone(), direction))
    }

    fn unlink_single<'a>(&'a self, link: &'a Node, direction: NodeLinkDirection) -> UnLinkReturnType<'a> {
        self.links.write().unwrap().remove(&NodeLink::new(link.none_clone(), direction))
    }

    fn get_link<'a>(&'a self, node_id: &'a NodeId, direction: NodeLinkDirection) -> GetLinkReturnType<'a> {
        match self.links.read().unwrap().get(&NodeLink::new(Node::from(node_id), direction)) {
            Some(link) => Some(link.node.clone()),
            None => None,
        }
    }

    fn destroy(&self) -> UnlinkAllReturnType {
        self.links.write().unwrap().clear();
    }

    fn get_debug_name(&self) -> String {
        // self.debug_name.clone()
        unimplemented!()
    }
}

impl Into<NodeInner> for NodeHashSet {
    fn into(self) -> NodeInner {
        NodeInner {
            data:       Arc::new(NodeHashSet::default()),
            functions:  NodeFunctions {
                links:          NodeHashSet::links,
                has_link:       NodeHashSet::has_link,
                link_single:    NodeHashSet::link_single,
                unlink_single:  NodeHashSet::unlink_single,
                get_link:       NodeHashSet::get_link,
                destroy:        NodeHashSet::destroy,
                get_debug_name: NodeHashSet::get_debug_name,
            },
            debug_name: std::any::type_name::<NodeHashSet>().to_string(),
            node_id: unsafe { crate::ID_GENERATOR.get_id() },
        }
    }
}

impl Into<Node> for NodeHashSet {
    fn into(self) -> Node {
        let inner_node: NodeInner = self.into();
        inner_node.into()
    }
}
