use std::{marker::PhantomData, sync::Arc};

use anyhow::{anyhow, Error};

use crate::{
    node::{node_id::NodeIdEnumType, Node, NodeDynamicFunctions, NodeLinkDirection},
    BOOKMARKS,
};

// Fixed using:
// https://stackoverflow.com/questions/34922579/how-to-take-ownership-of-anydowncast-ref-from-trait-object
// https://stackoverflow.com/questions/59824215/how-to-move-the-concrete-value-out-of-a-boxdyn-any
pub struct ObjectNodeType<'a, T>(&'a Node, PhantomData<T>);

impl<'a, T: 'static + Send + Sync> ObjectNodeType<'a, T> {
    pub fn get_object(&'a self) -> Arc<T> {
        let mut node_pointer_links = self
            .0
            .links()
            .filter(|link| link.node.get_id().node_type == NodeIdEnumType::Pointer);
        let data_node = node_pointer_links
            .next()
            .expect("No pointer nodes found!")
            .node;
        assert!(node_pointer_links.next().is_none());

        let any_arc_data = data_node.get_data_arc();
        let arc_data = Arc::downcast::<T>(any_arc_data).unwrap();
        arc_data
    }

    pub fn from(node: &'a Node) -> Result<Self, Error> {
        let object_node_type = unsafe { &BOOKMARKS.object_node_tag };
        if node.has_link(&object_node_type.0, NodeLinkDirection::From) {
            Ok(Self(node, PhantomData))
        } else {
            Err(anyhow!("Node is not a object node"))
        }
    }
}
