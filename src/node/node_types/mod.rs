// Note Node "Types" always have the signature of `stuct name(Node)`, and provide ease of use methods when casted from a Node that has a certain Node Tag.


pub mod link_node_tag;
pub mod object_node_type;
pub mod string_node_tag;

