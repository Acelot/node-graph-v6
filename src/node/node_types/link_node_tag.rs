use std::ptr::addr_of;

use crate::{
    node::Node,
    BOOKMARKS,
};
use anyhow::{anyhow, Error};

pub struct LinkNodeType {
    pub links_node:  Node,
    pub source_node: Node,
}

impl LinkNodeType {
    pub fn from(node: &Node) -> Result<Self, Error> {
        let bookmarks = unsafe { &*addr_of!(BOOKMARKS) };

        let mut links = node.get_links_with_link(&bookmarks.links_node_tag.0, crate::node::NodeLinkDirection::From);
        let links_node_option = links.next();
        assert!(links.next().is_none());
        match links_node_option {
            Some(link) => {
                return Ok(LinkNodeType {
                    links_node:  link.node,
                    source_node: node.clone(),
                })
            }
            None => return Err(anyhow!("Links node not found.")),
        }
    }

    // TODO: Remove/Deprecate
    // pub fn get_directional_to_links(&self) -> LinksFnReturnType{
    //     Box::new(
    //         self.links_node
    //             .links()
    //             .filter(|link| link.direction == NodeLinkDirection::To),
    //     )
    // }

    // pub fn get_directional_to_one_link(&self) -> Node {
    //     let mut links = self
    //         .links_node
    //         .links()
    //         .filter(|node| node != );
    //     let found_node = links
    //         .next()
    //         .expect("Error: No attached directional nodes found.");
    //     assert!(
    //         links.next().is_none(),
    //         "Error: Found more than one directional link."
    //     );
    //     found_node
    // }
}
