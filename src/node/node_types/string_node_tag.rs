use std::{ptr::addr_of, sync::Arc};

use crate::{
    node::{node_id::{NodeId, NodeIdTypes}, LinksFnReturnType, Node, NodeDynamicFunctions, NodeLinkDirection},
    BOOKMARKS,
};
use anyhow::{anyhow, Error};

use super::object_node_type::ObjectNodeType;

pub struct NameNodeType(pub Node);

impl NameNodeType {
    pub fn get_name_node(node: &Node) -> Result<Self, Error> {
        let mut links = node.get_links_with_link(unsafe { &BOOKMARKS.name_tag.0 }, NodeLinkDirection::From);

        let link_option = links.next();
        assert!(
            links.next().is_none(),
            "Error: Node has more than one name."
        );
        match link_option {
            Some(link) => Ok(Self(link.node)),
            None => Err(anyhow!("Node does not have a name node")),
        }
    }

    
    // Idea change String to NodeName(String) and use 
    // name_id_node.get_link_with_link(NodeId::fromType<NodeName(String)>)
    pub fn get_name(&self) -> String {
        let bookmarks = unsafe { &*addr_of!(BOOKMARKS) };
        let name_id_node = self.0.links().filter(|link|{link.node.get_id().node_type == NodeIdTypes::String || link.node.get_id().node_type == NodeIdTypes::RustType && link.direction == NodeLinkDirection::From}).next().unwrap().node;
        let object_type_node =name_id_node.get_link_with_link(&bookmarks.object_node_tag.0, NodeLinkDirection::From);
        let string: Arc<String> = ObjectNodeType::from(&object_type_node).unwrap().get_object();
        (&*string).clone()
    }

    pub fn from_name(name: &str) -> Result<Self, Error> {
        const ERROR_NAME_NOT_FOUND: &str = "Couldn't find name Node";

        let bookmarks = unsafe { &*addr_of!(BOOKMARKS) };
        match bookmarks.string_id_tag.0.get_link(&NodeId::from(name), NodeLinkDirection::To) {
            Some(node) => {
                return Ok(NameNodeType(
                    node.get_link_without_link(&bookmarks.name_tag.0, NodeLinkDirection::From),
                ))
            },
            None => return Err(anyhow!(ERROR_NAME_NOT_FOUND)),
        };
    }

    pub fn get_named_nodes(&self) -> LinksFnReturnType {
        self.0.get_nodes_directional(NodeLinkDirection::To)
    }
    pub fn get_named_node(&self) -> Node{
        self.0.get_node_directional(NodeLinkDirection::To)
    }
}
