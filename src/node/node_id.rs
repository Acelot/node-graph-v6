use std::{
    any::TypeId,
    hash::{DefaultHasher, Hash, Hasher},
};

pub type NodeIdNumericType = usize;

pub type NodeIdEnumType = NodeIdTypes;

#[derive(PartialEq, Eq, Hash, Clone, Copy, Debug)]
pub enum NodeIdTypes {
    Opaque,
    String,
    Pointer,
    RustType,
    Invaild, // Used as placeholder, otherwise, should never be used.
}

#[derive(PartialEq, Eq, Hash, Clone, Copy, Debug)]
pub struct NodeId {
    pub node_type:    NodeIdEnumType,
    pub node_numeric: NodeIdNumericType,
}

impl From<&str> for NodeId {
    fn from(value: &str) -> Self {
        let mut hasher = DefaultHasher::default();
        value.hash(&mut hasher);
        NodeId {
            node_type:    NodeIdTypes::String,
            node_numeric: hasher.finish() as usize,
        }
    }
}



impl NodeId {
    pub fn from_ref<T: 'static>(value: &T) -> NodeId {
    
        let value_ptr: usize = value as *const T as usize;

        let node_id = NodeId {
            node_type: NodeIdTypes::Pointer,
            node_numeric: value_ptr,
        };
        node_id
    }

    pub fn from_raw_pointer(value: *const ()) -> NodeId {
    
        

        let node_id = NodeId {
            node_type: NodeIdTypes::Pointer,
            node_numeric: value as usize,
        };
        node_id
    }

    pub fn from_type<T: 'static>() -> NodeId {
        let value = TypeId::of::<T>();
        let mut hasher = DefaultHasher::default();
        value.hash(&mut hasher);
        NodeId {
            node_type:    NodeIdTypes::RustType,
            node_numeric: hasher.finish() as usize,
        }
    }

    pub fn from_type_id(value: TypeId) -> NodeId {
        let mut hasher = DefaultHasher::default();
        value.hash(&mut hasher);
        NodeId {
            node_type:    NodeIdTypes::RustType,
            node_numeric: hasher.finish() as usize,
        }
    }

    pub(crate) fn new(node_type: NodeIdEnumType, node_numeric: NodeIdNumericType) -> Self {
        Self {
            node_type,
            node_numeric,
        }
    }
}
